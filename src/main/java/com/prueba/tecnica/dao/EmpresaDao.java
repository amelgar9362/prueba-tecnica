package com.prueba.tecnica.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prueba.tecnica.modelo.Empresa;

public interface EmpresaDao extends JpaRepository<Empresa, Long>{

}
