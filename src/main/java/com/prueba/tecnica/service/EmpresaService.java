package com.prueba.tecnica.service;

import java.util.List;

import com.prueba.tecnica.modelo.Empresa;
import com.prueba.tecnica.modelo.response.EmpresaResponse;

public interface EmpresaService {
	EmpresaResponse save(Empresa request);
	
	List<EmpresaResponse> findAll();
	
	List<EmpresaResponse> findAllUltimos();
	

}
