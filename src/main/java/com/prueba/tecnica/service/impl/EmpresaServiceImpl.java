package com.prueba.tecnica.service.impl;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prueba.tecnica.dao.EmpresaDao;
import com.prueba.tecnica.modelo.Empresa;
import com.prueba.tecnica.modelo.response.EmpresaResponse;
import com.prueba.tecnica.service.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService {
	
	@Autowired
	private EmpresaDao empresaDao;
	
	private ModelMapper model = new ModelMapper();

	@Override
	public EmpresaResponse save(Empresa request) {
		
		Empresa empresa =  empresaDao.save(request);
		EmpresaResponse response = model.map(empresa, EmpresaResponse.class);
		return response;
	}

	@Override
	public List<EmpresaResponse> findAll() {
		List<Empresa>  listEmpresa =  empresaDao.findAll();
		List<EmpresaResponse> response = model.map(listEmpresa, new TypeToken<List<EmpresaResponse>>() {
		}.getType());
		return response;
	}

	@Override
	public List<EmpresaResponse> findAllUltimos() {
		List<Empresa>  listEmpresa =  empresaDao.findAll();
		List<EmpresaResponse> response = model.map(listEmpresa, new TypeToken<List<EmpresaResponse>>() {
		}.getType());
		response=response.stream().sorted(Comparator.comparingLong(EmpresaResponse::getIdEmpresa).reversed()).limit(3).collect(Collectors.toList());
		return response;
	}

}
