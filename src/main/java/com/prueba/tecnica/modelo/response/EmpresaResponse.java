package com.prueba.tecnica.modelo.response;

import lombok.Data;

@Data
public class EmpresaResponse {
	private Long idEmpresa;
	
	private String ruc;
	
	private String razonSocial;
	
	private String direccion;
	
	private String estado;

}
