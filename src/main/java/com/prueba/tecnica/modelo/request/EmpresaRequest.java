package com.prueba.tecnica.modelo.request;



import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class EmpresaRequest {
	
	private Long idEmpresa;
	@NotNull(message = "el ruc no puede ser nulo y tampoco menor ni mayor a 11 digitos")
	@Min(value = 11)
	@Digits(fraction = 0, integer = 11)
	private String ruc;
	@NotNull(message = "La razon social no puede ser nulo")
	private String razonSocial;
	@NotNull(message = "La direccion no puede ser nulo")
	private String direccion;
	@NotEmpty(message = "El estado no debe ser vacio tampoco mayor a un caracter")
	@Max(value = 1)
	private String estado;

}
