package com.prueba.tecnica.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.tecnica.modelo.Empresa;
import com.prueba.tecnica.modelo.request.EmpresaRequest;
import com.prueba.tecnica.modelo.response.EmpresaResponse;
import com.prueba.tecnica.service.EmpresaService;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {
	
	@Autowired
	private EmpresaService service;
	
	ModelMapper model = new ModelMapper();
	@PostMapping
	public ResponseEntity<EmpresaResponse> save(@Valid @RequestBody EmpresaRequest request
			) {
		Empresa empresa = model.map(request, Empresa.class);
		EmpresaResponse response = service.save(empresa);
		return ResponseEntity
				.ok()
				.contentType(MediaType.APPLICATION_JSON).body(response);
	}
	
	@GetMapping
	public ResponseEntity<List<EmpresaResponse>> findAll() {
		List<EmpresaResponse> fx = service.findAll(); 	
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(fx);
	}
	
	@GetMapping("/ultimosRegistros")
	public ResponseEntity<List<EmpresaResponse>> findAll3ultimos() {
		List<EmpresaResponse> fx = service.findAllUltimos(); 	
		return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(fx);
	}

}
