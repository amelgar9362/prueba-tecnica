package com.prueba.tecnica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaTecnicaDeSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaTecnicaDeSpringBootApplication.class, args);
	}

}
