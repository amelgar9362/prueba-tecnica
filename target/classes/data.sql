drop table if exists rol CASCADE; 
drop table if exists Empresa CASCADE; 
drop table if exists usuario CASCADE; 
drop table if exists usuario_rol CASCADE;

create table Empresa (id_empresa number, ruc varchar(255), razon_social varchar(255), direccion varchar(255),estado char(1));
create table usuario (id_usuario varchar(255), clave varchar(255), estado char(1), nombre varchar(255));

create table rol (id_rol varchar(255), nombre varchar(255), descripcion varchar(255));

create table usuario_rol (id_usuario varchar(255), id_rol varchar(255));

INSERT INTO usuario(id_usuario, nombre, clave, estado) values (1, 'miguel_9362@hotmail.com', '$2a$10$ju20i95JTDkRa7Sua63JWOChSBc0MNFtG/6Sps2ahFFqN.HCCUMW.', '1');
INSERT INTO usuario(id_usuario, nombre, clave, estado) values (2, 'amelgar9362@gmail.com', '$2a$10$ju20i95JTDkRa7Sua63JWOChSBc0MNFtG/6Sps2ahFFqN.HCCUMW.', '1');

INSERT INTO rol (id_rol, nombre, descripcion) VALUES (1, 'ADMIN', 'Administrador');
INSERT INTO rol (id_rol, nombre, descripcion) VALUES (2, 'USER', 'Usuario');
INSERT INTO rol (id_rol, nombre, descripcion) VALUES (3, 'DBA', 'Admin de bd');

INSERT INTO usuario_rol (id_usuario, id_rol) VALUES (1, 1);
INSERT INTO usuario_rol (id_usuario, id_rol) VALUES (1, 3);
INSERT INTO usuario_rol (id_usuario, id_rol) VALUES (2, 2);